from django.shortcuts import render
from receipts.models import Receipt


def Receipt_list(request):
    receipt_list = Receipt.objects.all()
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)


# Create your views here.
